# HEADER: Hello World
# PREFIX: "CUSTOM"

begin remote

	name CUSTOM_KEY_POWER
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	26874
	begin raw_codes
		name KEY_POWER
			2367 579 1183 579 579 579 1183 579
			579 579 1183 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579   
	end raw_codes		

end remote

begin remote

	name CUSTOM_KEY_0
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	27400
	

	begin raw_codes
		name KEY_0
			2367 579 1183 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579  
	end raw_codes		

end remote

begin remote

	name CUSTOM_KEY_1
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	27400
	

	begin raw_codes
		name KEY_1
			2367 579 579 579 579 579 579 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579  
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_2
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	27400
	

	begin raw_codes
		name KEY_2
			2367 579 1183 579 579 579 579 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579   
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_3
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	27400
	

	begin raw_codes
		name KEY_3
			2367 579 579 579 1183 579 579 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579  
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_4
	flags	RAW_CODES
	eps	30
	aeps	100
	frequency	38029
	gap	27400
	

	begin raw_codes
		name KEY_4
			2367 579 1183 579 1183 579 579 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579 
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_5
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_5
			2367 579 1183 579 1183 579 579 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579  
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_6
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_6
			2367 579 1183 579 579 579 1183 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579  
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_7
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_7
			2340 579 579 579 1183 579 1183 579
			579 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579 
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_8
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_8
			2340 579 1157 579 1157 579 1157 579
			579 579 579 579 579 579 579 579
			1157 579 579 579 579 579 579 579
			579  
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_9
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_9
			2340 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_CHANNEL_PLUS
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_CHANNEL_PLUS
			2373 587 587 587 587 587 587 587
			587 587 1187 587 587 587 587 587
			1187 587 587 587 587 587 587 587
			587 
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_CHANNEL_MINUS
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_CHANNEL_MINUS
			2367 579 1183 579 579 579 579 579
			579 579 1183 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_ENTER
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	begin raw_codes
		name KEY_ENTER
			2367 579 1183 579 579 579 1183 579
			579 579 579 579 1183 579 1183 579
			1183 579 579 579 579 579 579 579
			579
	end raw_codes
end remote


begin remote

	name CUSTOM_KEY_VOLUME_PLUS
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_VOLUMEUP
			2371 585 585 585 1188 585 585 585
			585 585 1188 585 585 585 585 585
			1188 585 585 585 585 585 585 585
			585
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_VOLUME_MINUS
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_VOLUMEDOWN
			2367 579 1183 579 1183 579 579 579
			579 579 1183 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579 
	end raw_codes

end remote

begin remote

	name CUSTOM_KEY_MUTE
	flags RAW_CODES
	eps 30
	aeps 100
	frequency	38381
	gap	27400
	

	begin raw_codes
		name KEY_MUTE
			2340 579 579 579 579 579 1183 579
			579 579 1183 579 579 579 579 579
			1183 579 579 579 579 579 579 579
			579
	end raw_codes

end remote
