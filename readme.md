Cara capture LIRC

Prerequisite:
1. Install software LogicAnalyzer Saleae
2. Install software IrScrutinizer
3. Clone repo "https://gitlab.com/husnan.achmad/lirc-library.git"

A. Capture pakai software saleae logic
1. Atur settingan untuk pertama kali ke:
- config chanel 0 ke triger on rising edge
- terus tanda panah samping start, atur time period 1 MS/S
2. Dekatkan IR remote hingga menempel ke IR receiver
3. tekan "Start" atau ctrl+R untuk mulai mengcapture
4. Pencet tombol yang akan dicapture. List tombol yg di capture:
(channel 0-9, power, volume +/-)
5. Hasil bentuk gelombang harus bagus ga putus2 (terlihat carriernya) dan di ambil minimal satu gelombang hingga dua gelombang. Jika pendek bisa dua/tiga gelombang
6. Kemudian di export dengan shortcut ctrl+E
7. Simpan di folder baru dengan nama yang sesuai dengan kode remote

B. Export LIRC
1. Buka file template menyimpan macro: format2.xlsm. 
Pastikan paling update dg menjalankan perintah:
>> git pull origin master
2. Buka hasil capture logic analyzer  
3. Running irscrutinizer, setting export ke format Lirc dan uncheck automatic file name
4. Letakkan posisi kursor di kolom J3 dan running macro dg shortcut ctrl+w
5. Kembali ke irscrutinizer, klik kanan paste(replacing) 
6. Kondisikan waveform agar sesuai. Biasanya dg menghapus digit angka paling terakhir
7. Scrutinize
8. export
9. simpan di folder baru dg penamaan (0-9,POWER, VOLUME_PLUS, VOLUME_MINUS)
10. merge file dg running cmnd via git bash pastikan di folder benar.
laptop putih: "cd /c/Users/asus/Desktop/lirc/irscrutinizer/lirc-library"

running command "./convdata.sh" 
11. Masukan nama folder nomor kode remot yang mau di merge
12. Setelah running di edit pakai notepad++ 
-- Rename file menjadi "kode_remote.conf" ex: 075.conf
-- gap harus selalu di isi
-- KEY_VOLUME_PLUS harus di ganti jadi KEY_VOLUMEUP
-- KEY_VOLUME_MINUS harus di ganti jadi KEY_VOLUMEDOWN
-- SAVE
4.upload dg git
CARA 1 bisa melalui command 
./gitcommit.sh

CARA 2 

-- coment "git add --all"
-- git commit -m "add (nama folder)"
-- git push origin master