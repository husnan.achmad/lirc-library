echo "#begin my convert" > lirc.conf

for i in `find . -name "*.conf"`; do 
	index=$(echo $i | cut -d. -f2 | cut -d/ -f2)
	cat $i | sed "s/IrScrutinizerExport/CUSTOM_KEY_$index/" | sed "s/IrScrutinizer captured signal/KEY_$index/" >> lirc.conf
done;